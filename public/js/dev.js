$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": 'http://cihmvc_freelancer.local/index.php/base/api',
        "searching": true,
        "columns": [
            { "data": "Ho_ten" },
            { "data": "Ngay_sinh" },
            { "data": "Gioi_tinh" },
            { "data": "Balancer" },
            { "data": "Date_create" },
        ]
    } );

    $("#submit").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "http://cihmvc_freelancer.local/index.php/base/api",
            data: {
                hoten: $("#Ho_ten").val(),
                ngaysinh: $("#Ngay_sinh").val(),
                gioitinh: $("#Gioi_tinh").val(),
                balancer: $("#Balancer").val(),
            },
            success: function (result) {
                //alert(result.message);
                if(result.message == "success"){
                    $("#message").html('<div class="alert alert-success">'+result.message+'</div>');
                    $("#Ho_ten").val("");
                    $("#Ngay_sinh").val("");
                    $("#Gioi_tinh").val("Nam");
                    $("#Balancer").val("");
                    location.reload(true);
                }
                else{
                    $("#message").html('<div class="alert alert-danger">'+result.message+'</div>');
                }
            },
            error: function (result) {
                //alert(result.message);
                $("#message").html('<div class="alert alert-danger">'+result.message+'</div>');
            }
        });
    });

} );